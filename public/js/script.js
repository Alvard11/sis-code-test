$(document).on('click', '.upload_file', function(e){
    e.preventDefault();

    $.ajax({
        url: "/uploadFile",
        data:new FormData($("#UploadData")[0]),
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(response){
            location.reload()
        },
    });
});
