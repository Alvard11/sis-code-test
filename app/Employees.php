<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'employees';
    protected $fillable = [
        'user_id','date', 'category', 'employee_name', 'employee_address', 'expense_description', 'pre_tax_amount','tax_amount'
    ];
}
