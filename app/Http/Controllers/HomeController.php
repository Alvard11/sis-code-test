<?php

namespace App\Http\Controllers;

use App\Employees;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {

        $users = User::where('id','!=',Auth::id())->get();
        $employees =Employees::where('user_id',Auth::id())->paginate(10);

        return view('home', ['users' => $users, 'employees' => $employees]);
    }

}
