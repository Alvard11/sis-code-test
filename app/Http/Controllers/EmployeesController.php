<?php

namespace App\Http\Controllers;

use App\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class EmployeesController extends Controller
{
    public function upload_file(Request $request) {

        if ($request->hasFile('upload_file')) {

            $content = File::get($request->upload_file->path());
            $lines = explode("\r",$content);
            unset($lines[0]);

            foreach ($lines as $key => $line){

                $employee= explode("|",$line);

                Employees::create([
                    'user_id' => Auth::id(),
                    'date' => date("Y-m-d", strtotime($employee[0])),
                    'category' => $employee[1],
                    'employee_name' => $employee[2],
                    'employee_address' => $employee[3],
                    'expense_description' => $employee[4],
                    'pre_tax_amount' => $employee[5],
                    'tax_amount' => $employee[6]
                ]);
            }
        }
    }

    public function employees_user( $id = null ) {

        if (!is_null($id)) {
            $employees = Employees::where('user_id',$id)->paginate(10);
            return view('employees.employees_index',['employees'=>$employees]);
        }
    }

}
