<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Address</th>
        <th scope="col">Date</th>
        <th scope="col">Category</th>
        <th scope="col">Description</th>
        <th scope="col">Pre Tax Amount</th>
        <th scope="col">Tax Amount</th>
    </tr>
    </thead>
    <tbody>
    @forelse($employees as $employee)
        <tr>
            <td>{{$employee->employee_name}}</td>
            <td>{{$employee->employee_address}}</td>
            <td>{{$employee->date}}</td>
            <td>{{$employee->category}}</td>
            <td>{{$employee->expense_description}}</td>
            <td>{{$employee->pre_tax_amount}}</td>
            <td>{{$employee->tax_amount}}</td>
        </tr>
    @empty
        <tr>
           <td> <h1>No Result</h1></td>
        </tr>

    @endforelse


    </tbody>
</table>
{{ $employees->links() }}
