@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="col-md text-right pr-3 mb-2">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
            Import data
        </button>
    </div>

    <div class="row">
        @if(Auth::user()->type == 'admin')
            <div class="col-md-2">
                <div class="card-header bg-primary text-white"><h4>Users</h4></div>
                <div class="list-group">
                    @foreach($users as $user)
                        <a href="{{url('employees/user/'.$user->id)}}" class="list-group-item list-group-item-action">
                            {{$user->username}}
                        </a>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="col-md">
            @include('employees')
        </div>

    </div>
</div>


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="UploadData" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Import</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                    @csrf
                    <input type="file" name="upload_file"  accept=".psv">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary upload_file">Import</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
