# Sis code

====================================
About Laravel Framework

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

Simple, fast routing engine.
Powerful dependency injection container.
Multiple back-ends for session and cache storage.
Expressive, intuitive database ORM.
Database agnostic schema migrations.
Robust background job processing.
Real-time event broadcasting.
Laravel is accessible, powerful, and provides tools required for large, robust applications.

====================================
Project Installation
-------------------

Create .env file in the main directory of the project using the file called .env.example
Run `composer update` command via command line
Run `php artisan key:generate` command via command line
Run `php artisan migrate` command via command line 

Before the last command you need to have a DB created already and updated .env file
This command will migrate the DB with the required table
After these steps you can run the `php artisan serve` command and open your project with the `http://127.0.0.1:8000` link

In the users table there is an user registered already which is `admin`

Login credentials for `admin` user are following:

`Email address:` admin@admin.com
`Password:` admin

You can register as `user` as well using the default registration page

After successfully register / login you can click on the `Import Data` button and choose the `data_simple.psv` file from the main directory of the project for importing the data ( both for `user` and `admin` users ). The different between two accounts types is that in `admin` account you can see the imported data from the all users

====================================
About what I'm particularly proud in my implementation

I think that I'll not write much in this part as it was a simple test task. I like the Laravel framework and I just proud that the project is with Laravel. And this is the kind of confirmation of that Laravel can be used for all project types. Small, simple tasks and a huge projects as well